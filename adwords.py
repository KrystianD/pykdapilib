import time

import asyncio
import datetime
import json
import logging

from google.ads.googleads.client import GoogleAdsClient
# from googleads.common import ZeepServiceProxy
from typing import List, Dict, Union, Any, Set

import pytz
import yaml
# from googleads import adwords
from lxml import etree as ET

from lib import date_utils
from lib.cache import ICache
from lib.utils import normalize_phone, groupby_iter
from models import AdwordsCampaignDayData, AdwordsKeywordsDayData
from services.common.service_task import ServiceTask

API_VERSION = "v201809"


class AdWordsPartialError(Exception):
    def __init__(self, reason: str):
        self.reason = reason


def fetch_campaign_data(date) -> List[AdwordsCampaignDayData]:
    service = ServiceTask.instance

    client = adwords.AdWordsClient.LoadFromString(yaml.dump(service.get_config("adwords")))
    report_downloader = client.GetReportDownloader(version=API_VERSION)

    report_query = (
        'SELECT CampaignId, Impressions, Clicks, Cost, Conversions, AllConversions '
        'FROM CAMPAIGN_PERFORMANCE_REPORT '
        'DURING {0},{1}').format(date.strftime("%Y%m%d"), date.strftime("%Y%m%d"))

    d = report_downloader.DownloadReportAsStringWithAwql(report_query, "XML")

    data = []

    root = ET.fromstring(d.encode("utf-8"))
    for campaign in root.xpath("/report/table/row"):
        r = AdwordsCampaignDayData()
        r.date = date
        r.campaign_id = int(campaign.get("campaignID"))
        r.impressions = int(campaign.get("impressions"))
        r.clicks = int(campaign.get("clicks"))
        r.cost = float(campaign.get("cost")) / 1000000
        r.conversions = float(campaign.get("conversions"))
        r.all_conversions = float(campaign.get("allConv"))
        if r.impressions == 0:
            continue
        data.append(r)

    return data


def fetch_keywords_data(date) -> List[AdwordsKeywordsDayData]:
    service = ServiceTask.instance

    client = adwords.AdWordsClient.LoadFromString(yaml.dump(service.get_config("adwords")))
    report_downloader = client.GetReportDownloader(version=API_VERSION)

    report_query = (
        'SELECT Id, Criteria, Impressions, Clicks, Cost, Conversions, AllConversions '
        'FROM KEYWORDS_PERFORMANCE_REPORT '
        'DURING {0},{1}').format(date.strftime("%Y%m%d"), date.strftime("%Y%m%d"))

    d = report_downloader.DownloadReportAsStringWithAwql(report_query, "XML")

    xml_data = []

    root = ET.fromstring(d.encode("utf-8"))
    for keyword in root.xpath("/report/table/row"):
        row = {
            "date": date,
            "keyword_id": int(keyword.get("keywordID")),
            "keyword": keyword.get("keyword"),
            "impressions": int(keyword.get("impressions")),
            "clicks": int(keyword.get("clicks")),
            "cost": float(keyword.get("cost")) / 1000000,
            "conversions": float(keyword.get("conversions")),
            "all_conversions": float(keyword.get("allConv")),
        }
        xml_data.append(row)

    data = []
    for keyword, items in groupby_iter(xml_data, lambda x: x["keyword"]):
        il = list(items)

        r = AdwordsKeywordsDayData()
        r.date = date
        r.keyword = keyword
        r.impressions = sum(x["impressions"] for x in il)
        r.clicks = sum(x["clicks"] for x in il)
        r.cost = sum(x["cost"] for x in il)
        r.conversions = sum(x["conversions"] for x in il)
        r.all_conversions = sum(x["all_conversions"] for x in il)
        if r.impressions == 0:
            continue
        data.append(r)

    return data


def parse_final_url(final_url: str):
    if final_url is not None and final_url != "--":
        final_url = json.loads(final_url)
        assert len(final_url) == 1
        if len(final_url) > 0:
            return final_url[0]
    else:
        return None


class CampaignReportEntry:
    def __init__(self, d):
        self.id = int(d.get("campaignID"))
        self.state = d.get("campaignState")
        self.name = d.get("campaign")

        self.impressions = int(d.get("impressions"))
        self.clicks = int(d.get("clicks"))
        self.cost = float(d.get("cost")) / 1000000
        self.conversions = float(d.get("conversions"))
        self.all_conversions = float(d.get("allConv"))


class AdGroupReportEntry:
    def __init__(self, d):
        self.id = int(d.get("adGroupID"))
        self.state = d.get("adGroupState")
        self.name = d.get("adGroup")
        self.campaign_id = int(d.get("campaignID"))
        self.campaign_name = d.get("campaign")

        self.campaign_state = d.get("campaignState")

        self.impressions = int(d.get("impressions"))
        self.clicks = int(d.get("clicks"))
        self.cost = float(d.get("cost")) / 1000000
        self.conversions = float(d.get("conversions"))
        self.all_conversions = float(d.get("allConv"))


class AdReportEntry:
    def __init__(self, d):
        self.id = int(d.get("adID"))
        self.state = d.get("adState")
        self.ad_group_id = int(d.get("adGroupID"))
        self.ad_group_name = d.get("adGroup")
        self.campaign_id = int(d.get("campaignID"))
        self.campaign_name = d.get("campaign")

        self.campaign_state = d.get("campaignState")
        self.adgroup_state = d.get("adGroupState")

        self.headline = d.get("ad")
        self.description = d.get("description")
        self.line1 = d.get("headline1")
        self.line2 = d.get("headline2")
        self.final_url = parse_final_url(d.get("finalURL"))

        self.impressions = int(d.get("impressions"))
        self.clicks = int(d.get("clicks"))
        self.cost = float(d.get("cost")) / 1000000
        self.conversions = float(d.get("conversions"))
        self.all_conversions = float(d.get("allConv"))

        self.ctr = float(d.get("ctr").rstrip("%")) / 100
        self.conversion_rate = float(d.get("convRate").rstrip("%")) / 100
        self.all_conversion_rate = float(d.get("allConvRate").rstrip("%")) / 100


class KeywordReportEntry:
    def __init__(self, d):
        self.id = int(d.get("keywordID"))
        self.state = d.get("keywordState")
        self.keyword = d.get("keyword")
        self.ad_group_id = int(d.get("adGroupID"))
        self.ad_group_name = d.get("adGroup")
        self.campaign_id = int(d.get("campaignID"))
        self.campaign_name = d.get("campaign")

        self.campaign_state = d.get("campaignState")
        self.adgroup_state = d.get("adGroupState")

        self.final_url = parse_final_url(d.get("finalURL"))
        self.mobile_final_url = parse_final_url(d.get("mobileFinalURL"))
        self.app_final_url = parse_final_url(d.get("appFinalURL"))

        self.impressions = int(d.get("impressions"))
        self.clicks = int(d.get("clicks"))
        self.cost = float(d.get("cost")) / 100000
        self.conversions = float(d.get("conversions"))
        self.all_conversions = float(d.get("allConv"))


class AdWordsCallExtension:
    def __init__(self, cfg: 'AdWordsConfiguration'):
        self.cfg = cfg

        self.id: int = None
        self.phone_number: str = None

    @property
    def objects(self) -> List[Union['AdWordsCampaign', 'AdWordsAdGroup']]:
        objects = []

        for campaign in self.cfg.campaigns:
            if self.id in campaign.call_extensions_ids:
                objects.append(campaign)

        for adgroup in self.cfg.adgroups:
            if self.id in adgroup.call_extensions_ids:
                objects.append(adgroup)

        return objects


class AdWordsCampaign:
    def __init__(self, cfg: 'AdWordsConfiguration'):
        self.cfg = cfg

        self.id: int = None
        self.name: str = None
        self.status: str = None

        self.adgroups_ids: List[int] = []
        self.call_extensions_ids: List[int] = []

    @property
    def call_extensions(self) -> List[AdWordsCallExtension]:
        return [self.cfg.get_call_extension_by_id(x) for x in self.call_extensions_ids]

    def __str__(self):
        return "[Campaign #{}, {}]".format(self.id, self.name)

    def __repr__(self):
        return str(self)


class AdWordsAdGroup:
    def __init__(self, cfg: 'AdWordsConfiguration'):
        self.cfg = cfg

        self.id: int = None
        self.campaign_id: int = None
        self.name: str = None
        self.status: str = None

        self.call_extensions_ids: List[int] = []

    @property
    def campaign_name(self) -> str:
        return self.cfg.get_campaign_by_id(self.campaign_id).name

    @property
    def call_extensions(self) -> List[AdWordsCallExtension]:
        return [self.cfg.get_call_extension_by_id(x) for x in self.call_extensions_ids]

    def __str__(self):
        return "[AdGroup #{}, {}/{}]".format(self.id, self.campaign_name, self.name)

    def __repr__(self):
        return str(self)


class AdWordsReport:
    def __init__(self,
                 start_date: datetime.date,
                 end_date: datetime.date,
                 keywords_data: List[Any],
                 adgroups_data: List[Any],
                 ads_data: List[Any],
                 campaigns_data: List[Any]):
        self.start_date = start_date
        self.end_date = end_date
        self.keywords_data = keywords_data
        self.adgroups_data = adgroups_data
        self.ads_data = ads_data
        self.campaigns_data = campaigns_data

        self.keywords: List[KeywordReportEntry] = []
        self.adgroups: List[AdGroupReportEntry] = []
        self.ads: List[AdReportEntry] = []
        self.campaigns: List[CampaignReportEntry] = []

        for obj in self.keywords_data:
            self.keywords.append(KeywordReportEntry(obj))
        for obj in self.adgroups_data:
            self.adgroups.append(AdGroupReportEntry(obj))
        for obj in self.ads_data:
            self.ads.append(AdReportEntry(obj))
        for obj in self.campaigns_data:
            self.campaigns.append(CampaignReportEntry(obj))

    def is_finished(self):
        cur_date = date_utils.get_cur_pst_date()
        return self.end_date < cur_date - datetime.timedelta(days=7)

    def serialize(self):
        data = {
            "start_date": self.start_date,
            "end_date": self.end_date,
            "keywords_data": self.keywords_data,
            "adgroups_data": self.adgroups_data,
            "ads_data": self.ads_data,
            "campaigns_data": self.campaigns_data,
        }
        return data


class AdWordsConfiguration:
    def __init__(self):
        self.campaigns: List[AdWordsCampaign] = []
        self.adgroups: List[AdWordsAdGroup] = []
        self.call_extensions: List[AdWordsCallExtension] = []

        self.campaigns_by_id: Dict[int, AdWordsCampaign] = {}
        self.adgroups_by_id: Dict[int, AdWordsAdGroup] = {}

        self.call_extensions_by_id: Dict[int, AdWordsCallExtension] = {}
        self.call_extensions_by_number: Dict[str, List[AdWordsCallExtension]] = {}
        # self.call_extensions_to_objects: Dict[AdWordsCallExtension, Union[AdWordsCampaign,AdWordsAdGroup]] = {}

    def get_campaign_by_id(self, id: int) -> AdWordsCampaign:
        return self.campaigns_by_id[id]

    def get_adgroup_by_id(self, id: int) -> AdWordsAdGroup:
        return self.adgroups_by_id[id]

    def get_call_extension_by_id(self, id: int) -> AdWordsCallExtension:
        return self.call_extensions_by_id[id]

    def find_by_phone_number(self, number: str) -> Set[Union[AdWordsCampaign, AdWordsAdGroup]]:
        number = normalize_phone(number)

        objects = set()

        for campaign in self.campaigns:
            for call_ext in (self.get_call_extension_by_id(x) for x in campaign.call_extensions_ids):
                if call_ext.phone_number == number:
                    objects.add(campaign)

        for adgroup in self.adgroups:
            for call_ext in (self.get_call_extension_by_id(x) for x in adgroup.call_extensions_ids):
                if call_ext.phone_number == number:
                    objects.add(adgroup)

        return objects


class AdWordsAPI:
    def __init__(self, credentials: Union[str, Dict[str, Any]], cache: ICache = None):
        if not isinstance(credentials, dict):
            credentials = yaml.load(credentials)
        self.credentials = credentials
        self.client = None
        self.report_downloader = None
        self.cache = cache

        # self.report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        self.client = GoogleAdsClient.load_from_dict(credentials["adwords"])
        self.ga_service = self.client.get_service("GoogleAdsService")

    async def _do_service_mutate(self, service: str, operations: List[Dict], version: str):
        def fn():
            # if self.client is None:

            s = self.client.GetService(service, version=version)
            return s.mutate(operations)

        return await asyncio.get_event_loop().run_in_executor(None, fn)

    async def _do_service_get(self, service: str, selector: Dict, version: str):
        def fn():
            # if self.client is None:
            #     self.client = adwords.AdWordsClient.LoadFromString(self.credentials)
            #     self.client.cache = False
            #     self.report_downloader = self.client.GetReportDownloader(version=API_VERSION)

            s = self.client.GetService(service, version=version)
            return s.get(selector)

        return await asyncio.get_event_loop().run_in_executor(None, fn)

    async def _do_service_query(self, query: str):
        def fn():
            customer_id = self.credentials["adwords"]["client_customer_id"]
            stream = self.ga_service.search_stream(customer_id=customer_id.replace("-", ""), query=query)
            return [row for batch in stream for row in batch.results]

        return await asyncio.get_event_loop().run_in_executor(None, fn)

    async def _get_conversion_mapping(self) -> Dict[str, str]:
        resp = await self._do_service_query("SELECT conversion_action.name, conversion_action.id FROM conversion_action")
        return {x.conversion_action.name: x.conversion_action.resource_name for x in resp}

    async def upload_offline_conversion(self,
                                        gclid: str,
                                        conversion_name: str,
                                        conversion_time: datetime.datetime,
                                        conversion_value: int = None,
                                        attribution_model: str = None,
                                        attribution_credit: float = None):

        assert conversion_time.tzinfo == pytz.utc

        conversion_actions_mapping = await self._get_conversion_mapping()

        customer_id = self.credentials["adwords"]["client_customer_id"]
        conversion_action_id = conversion_actions_mapping[conversion_name]

        click_conversion = self.client.get_type("ClickConversion")
        click_conversion.conversion_action = conversion_action_id
        click_conversion.gclid = gclid

        if conversion_value is not None:
            click_conversion.conversion_value = float(conversion_value)
        click_conversion.conversion_date_time = conversion_time.strftime("%Y-%m-%d %H:%M:%S+00:00")
        click_conversion.currency_code = "USD"

        if attribution_model is not None and attribution_credit is not None:
            external_attribution_data = self.client.get_type("ExternalAttributionData")
            external_attribution_data.external_attribution_model = attribution_model
            external_attribution_data.external_attribution_credit = attribution_credit
            click_conversion.external_attribution_data = external_attribution_data

        logging.info("upload conversion: " + str(click_conversion))

        conversion_upload_service = self.client.get_service("ConversionUploadService")

        request = self.client.get_type("UploadClickConversionsRequest")
        request.customer_id = customer_id.replace("-", "")
        request.conversions = [click_conversion]
        request.partial_failure = True

        resp = conversion_upload_service.upload_click_conversions(request=request)

        if resp.partial_failure_error.code != 0:
            GoogleAdsFailure = type(self.client.get_type("GoogleAdsFailure"))
            assert len(resp.partial_failure_error.details) == 1
            failure_object = GoogleAdsFailure.deserialize(resp.partial_failure_error.details[0].value)

            assert len(failure_object.errors) == 1
            error_name = failure_object.errors[0].error_code.conversion_upload_error.name

            raise AdWordsPartialError(error_name)

        return resp

    async def upload_offline_call_conversion(self,
                                             caller_id: str,
                                             call_start_time: datetime.datetime,
                                             conversion_name: str,
                                             conversion_time: datetime.datetime = None,
                                             conversion_value: int = None):
        assert call_start_time.tzinfo == pytz.utc
        assert conversion_time is None or conversion_time.tzinfo == pytz.utc

        conversion_actions_mapping = await self._get_conversion_mapping()

        customer_id = self.credentials["adwords"]["client_customer_id"]
        conversion_action_id = conversion_actions_mapping[conversion_name]

        call_conversion = self.client.get_type("CallConversion")
        call_conversion.conversion_action = conversion_action_id
        call_conversion.caller_id = caller_id
        call_conversion.call_start_date_time = call_start_time.strftime("%Y-%m-%d %H:%M:%S+00:00")

        if conversion_value is not None:
            call_conversion.conversion_value = float(conversion_value)
        call_conversion.conversion_date_time = conversion_time.strftime("%Y-%m-%d %H:%M:%S+00:00")
        call_conversion.currency_code = "USD"

        logging.info("upload call conversion: " + str(call_conversion))

        conversion_upload_service = self.client.get_service("ConversionUploadService")

        request = self.client.get_type("UploadCallConversionsRequest")
        request.customer_id = customer_id.replace("-", "")
        request.conversions = [call_conversion]
        request.partial_failure = True

        resp = conversion_upload_service.upload_call_conversions(request=request)

        if resp.partial_failure_error.code != 0:
            GoogleAdsFailure = type(self.client.get_type("GoogleAdsFailure"))
            assert len(resp.partial_failure_error.details) == 1
            failure_object = GoogleAdsFailure.deserialize(resp.partial_failure_error.details[0].value)

            assert len(failure_object.errors) == 1
            error_name = failure_object.errors[0].error_code.conversion_upload_error.name

            raise AdWordsPartialError(error_name)

        return resp

    async def update_offline_conversion(self,
                                        gclid: str,
                                        conversion_name: str,
                                        conversion_time: datetime.datetime,
                                        adjustment_time: datetime.datetime,
                                        conversion_value: int = None):

        assert conversion_time.tzinfo == pytz.utc
        assert adjustment_time.tzinfo == pytz.utc

        conversion_actions_mapping = await self._get_conversion_mapping()

        customer_id = self.credentials["adwords"]["client_customer_id"]
        conversion_action_id = conversion_actions_mapping[conversion_name]

        conversion_adjustment = self.client.get_type("ConversionAdjustment")
        conversion_adjustment.conversion_action = conversion_action_id
        conversion_adjustment.adjustment_type = self.client.enums.ConversionAdjustmentTypeEnum["RESTATEMENT"].value
        conversion_adjustment.adjustment_date_time = adjustment_time.strftime("%Y-%m-%d %H:%M:%S+00:00")

        conversion_adjustment.gclid_date_time_pair.gclid = gclid
        conversion_adjustment.gclid_date_time_pair.conversion_date_time = conversion_time.strftime("%Y-%m-%d %H:%M:%S+00:00")

        conversion_adjustment.restatement_value.adjusted_value = float(conversion_value)

        conversion_adjustment_upload_service = self.client.get_service("ConversionAdjustmentUploadService")
        request = self.client.get_type("UploadConversionAdjustmentsRequest")
        request.customer_id = customer_id.replace("-", "")
        request.conversion_adjustments = [conversion_adjustment]
        request.partial_failure = True

        resp = conversion_adjustment_upload_service.upload_conversion_adjustments(request=request)

        if resp.partial_failure_error.code != 0:
            GoogleAdsFailure = type(self.client.get_type("GoogleAdsFailure"))
            assert len(resp.partial_failure_error.details) == 1
            failure_object = GoogleAdsFailure.deserialize(resp.partial_failure_error.details[0].value)

            assert len(failure_object.errors) == 1
            error_name = failure_object.errors[0].error_code.conversion_adjustment_upload_error.name

            raise AdWordsPartialError(error_name)

        return resp

    async def load_configuration(self) -> AdWordsConfiguration:
        cfg = AdWordsConfiguration()

        campaigns_resp, adgroups_resp, campaign_feed_resp, adgroup_feed_resp = await asyncio.gather(
                self._do_service_query("""
                SELECT
                  campaign.id,
                  campaign.name,
                  campaign.status
                FROM campaign
                WHERE campaign.status IN ('ENABLED', 'PAUSED', 'REMOVED')
                ORDER BY campaign.id"""),
                self._do_service_query("""
                SELECT
                  campaign.id,
                  ad_group.id,
                  ad_group.name,
                  ad_group.status
                FROM ad_group
                ORDER BY ad_group.id"""),
                self._do_service_query("""
                SELECT
                  campaign.id,
                  feed.id,
                  campaign_feed.placeholder_types,
                  campaign_feed.matching_function.left_operands,
                  campaign_feed.matching_function.right_operands
                FROM campaign_feed
                WHERE
                  campaign_feed.placeholder_types CONTAINS ALL ('CALL') AND
                  campaign_feed.status = 'ENABLED'
            """),
                self._do_service_query("""
                SELECT
                  ad_group.id,
                  feed.id,
                  ad_group_feed.placeholder_types,
                  ad_group_feed.matching_function.left_operands,
                  ad_group_feed.matching_function.right_operands
                FROM ad_group_feed
                WHERE
                  ad_group_feed.placeholder_types CONTAINS ALL ('CALL') AND
                  ad_group_feed.status = 'ENABLED'
            """)
        )

        for data in campaigns_resp:
            campaign = AdWordsCampaign(cfg)
            campaign.id = data.campaign.id
            campaign.name = data.campaign.name
            campaign.status = data.campaign.status.name
            cfg.campaigns.append(campaign)
            cfg.campaigns_by_id[campaign.id] = campaign

        for data in adgroups_resp:
            adgroup = AdWordsAdGroup(cfg)
            adgroup.id = data.ad_group.id
            adgroup.campaign_id = data.campaign.id
            adgroup.name = data.ad_group.name
            adgroup.status = data.ad_group.status.name
            cfg.adgroups.append(adgroup)
            cfg.adgroups_by_id[adgroup.id] = adgroup

        feed_ids = set()
        all_feed_item_ids = set()

        for e in campaign_feed_resp:
            campaign_id = e.campaign.id
            feed_id = e.feed.id

            assert e.campaign_feed.matching_function.left_operands[0].request_context_operand.context_type.name == "FEED_ITEM_ID"
            feed_item_ids = [x.constant_operand.long_value for x in e.campaign_feed.matching_function.right_operands]

            feed_ids.add(feed_id)
            all_feed_item_ids |= set(feed_item_ids)

            campaign = cfg.get_campaign_by_id(campaign_id)
            campaign.call_extensions_ids = feed_item_ids

        for e in adgroup_feed_resp:
            adgroup_id = e.ad_group.id
            feed_id = e.feed.id

            assert e.ad_group_feed.matching_function.left_operands[0].request_context_operand.context_type.name == "FEED_ITEM_ID"
            feed_item_ids = [x.constant_operand.long_value for x in e.ad_group_feed.matching_function.right_operands]

            feed_ids.add(feed_id)
            all_feed_item_ids |= set(feed_item_ids)

            adgroup = cfg.get_adgroup_by_id(adgroup_id)
            adgroup.call_extensions_ids = feed_item_ids

        assert len(feed_ids) == 1
        feed_id = list(feed_ids)[0]

        feed_mapping_resp, feed_item_resp = await asyncio.gather(
                self._do_service_query(f"""
                SELECT 
                  feed_mapping.attribute_field_mappings 
                FROM feed_mapping 
                WHERE 
                  feed.id = {feed_id} 
                  AND feed_mapping.placeholder_type = 'CALL' 
            """),
                self._do_service_query(f"""
                SELECT 
                  feed_item.attribute_values, 
                  feed_item.id 
                FROM feed_item 
                WHERE 
                  feed.id = {feed_id} AND
                  feed_item.id IN ({','.join(str(x) for x in all_feed_item_ids)}) 
            """)
        )

        feed_attribute_id = None
        for e in feed_mapping_resp:
            for mapping in e.feed_mapping.attribute_field_mappings:
                if mapping.field_id == 1:
                    feed_attribute_id = mapping.feed_attribute_id
                    break

        assert feed_attribute_id is not None

        for e in feed_item_resp:
            for attr in e.feed_item.attribute_values:
                if attr.feed_attribute_id == feed_attribute_id:
                    call_ext = AdWordsCallExtension(cfg)
                    call_ext.id = e.feed_item.id
                    call_ext.phone_number = normalize_phone(attr.string_value)

                    cfg.call_extensions.append(call_ext)
                    cfg.call_extensions_by_id[call_ext.id] = call_ext

                    if call_ext.phone_number not in cfg.call_extensions_by_number:
                        cfg.call_extensions_by_number[call_ext.phone_number] = []
                    cfg.call_extensions_by_number[call_ext.phone_number].append(call_ext)

        return cfg

    def _do_query(self, q: str):
        # if self.client is None:
        #     self.client = adwords.AdWordsClient.LoadFromString(self.credentials)
        #     self.client.cache = False
        #     self.report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        return self.report_downloader.DownloadReportAsStringWithAwql(q, "XML").encode("utf-8")

    def _do_query_details(self,
                          fields: List[str],
                          report: str,
                          date_start: datetime.date,
                          date_end: datetime.date):

        cur_date = date_utils.get_cur_pst_date()
        is_finished = date_end < cur_date - datetime.timedelta(days=7)

        query = 'SELECT {fields} FROM {report} DURING {0},{1}'.format(
                date_start.strftime("%Y%m%d"),
                date_end.strftime("%Y%m%d"),
                report=report,
                fields=",".join(fields))

        data = None
        if self.cache is not None:
            data = self.cache.get(query)

        if data is None:
            data = self._do_query(query)

            if self.cache is not None:
                if is_finished:
                    self.cache.set(query, data, ttl=None)
                else:
                    self.cache.set(query, data, ttl=datetime.timedelta(days=1))

        root = ET.fromstring(data, parser=ET.XMLParser(huge_tree=True))
        objects = root.xpath("/report/table/row")
        return objects

    def get_keywords(self, date_start: datetime.date, date_end: datetime.date) -> List[KeywordReportEntry]:
        data = self.get_keywords_data(date_start, date_end)
        objects = []
        for obj in data:
            objects.append(KeywordReportEntry(obj))
        return objects

    def get_ad_groups(self, date_start: datetime.date, date_end: datetime.date) -> List[AdGroupReportEntry]:
        data = self.get_ad_groups_data(date_start, date_end)
        objects = []
        for obj in data:
            objects.append(AdGroupReportEntry(obj))
        return objects

    def get_ads(self, date_start: datetime.date, date_end: datetime.date) -> List[AdReportEntry]:
        data = self.get_ads_data(date_start, date_end)
        objects = []
        for obj in data:
            objects.append(AdReportEntry(obj))
        return objects

    def get_campaigns(self, date_start: datetime.date, date_end: datetime.date) -> List[CampaignReportEntry]:
        data = self.get_campaigns_data(date_start, date_end)
        objects = []
        for obj in data:
            objects.append(CampaignReportEntry(obj))
        return objects

    def get_keywords_data(self, date_start: datetime.date, date_end: datetime.date) -> List[Any]:
        return self._do_query_details(
                fields=["Id", "Status", "CampaignStatus", "AdGroupStatus", "AdGroupId", "AdGroupName",
                        "CampaignId", "CampaignName", "FinalUrls", "FinalAppUrls", "FinalMobileUrls",
                        "Criteria", "Impressions", "Clicks", "Cost", "Conversions", "AllConversions"],
                report="KEYWORDS_PERFORMANCE_REPORT",
                date_start=date_start,
                date_end=date_end)

    def get_ad_groups_data(self, date_start: datetime.date, date_end: datetime.date) -> List[Any]:
        return self._do_query_details(
                fields=["AdGroupId", "CampaignStatus", "AdGroupStatus", "AdGroupName", "CampaignId",
                        "CampaignName", "Impressions", "Clicks", "Cost", "Conversions", "AllConversions"],
                report="ADGROUP_PERFORMANCE_REPORT",
                date_start=date_start,
                date_end=date_end)

    def get_ads_data(self, date_start: datetime.date, date_end: datetime.date) -> List[Any]:
        return self._do_query_details(
                fields=["Id", "Status", "CampaignStatus", "AdGroupStatus", "AdGroupId",
                        "AdGroupName", "CampaignId", "CampaignName", "Headline", "HeadlinePart1",
                        "HeadlinePart2", "Description", "CreativeFinalUrls", "Impressions", "Clicks",
                        "Cost", "Conversions", "AllConversions", "Ctr", "AllConversionRate", "ConversionRate"],
                report="AD_PERFORMANCE_REPORT",
                date_start=date_start,
                date_end=date_end)

    def get_campaigns_data(self, date_start: datetime.date, date_end: datetime.date) -> List[Any]:
        return self._do_query_details(
                fields=["CampaignId", "CampaignStatus", "CampaignName",
                        "Impressions", "Clicks", "Cost",
                        "Conversions", "AllConversions"],
                report="CAMPAIGN_PERFORMANCE_REPORT",
                date_start=date_start,
                date_end=date_end)

    def get_full_report(self, start_date: datetime.date, end_date: datetime.date):
        report = AdWordsReport(
                start_date=start_date,
                end_date=end_date,
                campaigns_data=self.get_campaigns_data(start_date, end_date),
                keywords_data=self.get_keywords_data(start_date, end_date),
                adgroups_data=self.get_ad_groups_data(start_date, end_date),
                ads_data=self.get_ads_data(start_date, end_date),
        )
        return report
