import datetime
from typing import Union, List

import aiohttp
from aiohttp import ContentTypeError


class CallRailInvalidResponseException(Exception):
    pass


class CallRailNumber:
    def __init__(self, data):
        self.id: int = data["id"]
        self.name: str = data["name"]
        self.type: str = data["type"]
        self.status: str = data["status"]
        self.destination_number: str = data["destination_number"]
        self.tracking_numbers: str = data["tracking_numbers"]

    @property
    def tracking_number(self):
        if len(self.tracking_numbers) != 1:
            raise ValueError("0 or more numbers")
        return self.tracking_numbers[0]

    def is_active(self):
        return self.status == "active"

    def __repr__(self):
        return "[CallRailNumber {} {} {}]".format(self.type, self.name, self.tracking_numbers)


class CallRail:
    def __init__(self, token: str):
        self.token = token

    async def get_numbers(self) -> List[CallRailNumber]:
        crnumbers = await self._do_request_paged("/trackers.json", {}, "trackers")
        # print(crnumbers)
        return [CallRailNumber(x) for x in crnumbers]

    async def get_page_views_urls(self, call_id: str):
        views = await self._do_request("/calls/{}/page_views.json".format(call_id), {})

        return [x["page_url"] for x in views["page_views"]]

    async def get_call_integration_data(self, call_id: str):
        call = await self._do_request("/calls/{}.json".format(call_id), {
            "fields": "integration_data",
        })

        return call["integration_data"]

    async def get_call(self, call_id: Union[int, str]):
        call = await self._do_request("/calls/{}.json".format(call_id), {})

        return call

    async def get_calls(self, from_: Union[datetime.date, datetime.datetime] = None):
        params = {}
        if from_ is not None:
            if isinstance(from_, datetime.date):
                params["start_date"] = from_.strftime("%Y-%m-%d")
            elif isinstance(from_, datetime.datetime):
                params["start_date"] = from_.strftime("%Y-%m-%dT%H:%M")

        additional_fields = [
            "tracker_id",
            "created_at",
            "keywords",
            "gclid",
            "referring_url",
            "landing_page_url",
            "last_requested_url",
            "medium",
            "source_name",
            "referrer_domain",
            "utm_campaign",
            "utm_source",
            "utm_medium",
            "utm_content",
            "utm_term",
            "ga",
        ]

        params["per_page"] = "250"
        params["sort"] = "start_time"
        params["fields"] = ",".join(additional_fields)

        page = 1
        total_pages = 1

        calls = []
        while page <= total_pages:
            params["page"] = str(page)
            resp = await self._do_request("/calls.json", params=params)
            if "total_pages" not in resp:
                raise CallRailInvalidResponseException()
            total_pages = int(resp["total_pages"])
            calls += resp["calls"]
            page += 1

        return calls

    async def _do_request_paged(self, url: str, params, res_item):
        page = 1
        total_pages = 1

        objects = []
        while page <= total_pages:
            params["page"] = str(page)
            resp = await self._do_request(url, params=params)
            if "total_pages" not in resp:
                raise CallRailInvalidResponseException()
            total_pages = int(resp["total_pages"])
            objects += resp[res_item]
            page += 1

        return objects

    async def _do_request(self, url: str, params=None):
        if params is None:
            params = {}

        headers = {
            "Authorization": "Token token=\"{}\"".format(self.token),
        }
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(f'https://api.callrail.com/v3/a/682219560{url}', params=params, headers=headers) as response:
                    return await response.json()
        except ContentTypeError:
            raise CallRailInvalidResponseException()

    async def _do_request_post(self, url: str, params=None, json=None):
        if params is None:
            params = {}

        headers = {
            "Authorization": "Token token=\"{}\"".format(self.token),
        }
        try:
            async with aiohttp.ClientSession() as session:
                async with session.post(f'https://api.callrail.com/v3/a/682219560{url}', params=params, json=json, headers=headers) as response:
                    return await response.json()
        except ContentTypeError:
            raise CallRailInvalidResponseException()
