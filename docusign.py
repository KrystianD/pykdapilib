import json
import logging
from typing import List, Any, TYPE_CHECKING

import aiohttp
import base64

if TYPE_CHECKING:
    from services.common.service_task import ServiceTask


class DocuSignException(Exception):
    def __init__(self, message, request, resp):
        super().__init__(message)
        self.request = request
        self.resp = resp


class DocuSignContractVoided(Exception):
    pass


class DocuSignEnvelopeRecipientData:
    def __init__(self):
        self.name = None  # type: str
        self.email = None  # type: str


class DocuSignEnvelopeTabData:
    def __init__(self):
        self.type = None  # type: str
        self.email = None  # type: str


class DocuSignEnvelopeSignerData:
    def __init__(self):
        self.name = None  # type: str
        self.email = None  # type: str
        self.email_body = None  # type: str
        self.send_email = True
        self.tabs = True


class DocuSignEnvelopeData:
    def __init__(self):
        self.document_data = None  # type: bytes
        self.document_name = None  # type: str
        self.subject = None  # type: str
        self.main_signer = None  # type: DocuSignEnvelopeSignerData
        self.clients = []  # type: List[DocuSignEnvelopeSignerData]
        self.cc = []  # type: List[DocuSignEnvelopeRecipientData]
        self.cc_completed = []  # type: List[DocuSignEnvelopeRecipientData]
        self.webhook = None  # type: str
        self.reply_to_name = None  # type: str
        self.reply_to_email = None  # type: str


class DocuSignNotFound(Exception):
    pass


class DocuSignAPI:
    def __init__(self, api_url: str, api_username: str, api_password: str, api_integrator_key: str):
        self.api_url = api_url
        self.api_username = api_username
        self.api_password = api_password
        self.api_integrator_key = api_integrator_key

    @staticmethod
    def create_from_service(service: 'ServiceTask'):
        return DocuSignAPI(service.get_config("docusign.api_url"),
                           service.get_config("docusign.username"),
                           service.get_config("docusign.password"),
                           service.get_config("docusign.integrator_key"))

    async def login(self):
        resp = await self._do_request("/v2/login_information")
        self.api_url = resp["loginAccounts"][0]["baseUrl"]

    async def create_envelope(self, env: DocuSignEnvelopeData, send: bool = False):
        recipientId = 0

        signers = []

        if env.main_signer is not None:
            recipientId += 1
            signers.append({
                "email": env.main_signer.email,
                "clientUserId": env.main_signer.email,
                "embeddedRecipientStartURL": "SIGN_AT_DOCUSIGN",
                "name": env.main_signer.name,
                "emailNotification": {
                    "emailSubject": env.subject,
                    "emailBody": env.main_signer.email_body,
                },
                "recipientId": recipientId,
                "routingOrder": 1,
                "tabs": env.main_signer.tabs,
            })

        SIGNATURE_WIDTH_PX = 180
        INITIALS_WIDTH_PX = 90
        for i, signer in enumerate(env.clients):
            recipientId += 1
            signers.append({
                "email": signer.email,
                "clientUserId": signer.email,
                "embeddedRecipientStartURL": "SIGN_AT_DOCUSIGN" if signer.send_email else None,
                "name": signer.name,
                "emailNotification": {
                    "emailSubject": env.subject,
                    "emailBody": signer.email_body,
                },
                "recipientId": recipientId,
                "routingOrder": 1,
                "tabs": {
                    "dateSignedTabs": [
                        {
                            "anchorString": "[Date]",
                            "anchorUnits": "pixels",
                            "anchorXOffset": -5 + i * SIGNATURE_WIDTH_PX,
                            "anchorYOffset": -30,
                            "documentId": "1",
                            "recipientId": recipientId,
                            "required": "true",
                        }
                    ],
                    "fullNameTabs": [
                        {
                            "anchorString": "[Print Name and Title]",
                            "anchorUnits": "pixels",
                            "anchorXOffset": -5 + i * SIGNATURE_WIDTH_PX,
                            "anchorYOffset": -30,
                            "documentId": "1",
                            "recipientId": recipientId,
                            "required": "true",
                        }
                    ],
                    "signHereTabs": [
                        {
                            "anchorString": "[Signature]",
                            "anchorUnits": "pixels",
                            "anchorXOffset": 5 + i * SIGNATURE_WIDTH_PX,
                            "anchorYOffset": -30,
                            "documentId": "1",
                            "recipientId": recipientId,
                            "required": "true",
                        }
                    ],
                    "initialHereTabs": [
                        {
                            "anchorString": "SIGNERS INITIALS:",
                            "anchorUnits": "pixels",
                            "anchorXOffset": 5 + i * INITIALS_WIDTH_PX,
                            "anchorYOffset": 40,
                            "documentId": "1",
                            "recipientId": recipientId,
                            "required": "true",
                        }
                    ],
                    "textTabs": [
                        {
                            "anchorString": "Phone:",
                            "anchorUnits": "pixels",
                            "anchorXOffset": 42,
                            "anchorYOffset": -3,
                            "width": 100,
                            "height": 21,
                            "tabLabel": "Phone",
                            "name": "Phone",
                            "documentId": "1",
                            "recipientId": recipientId,
                            "required": "true",
                            "anchorIgnoreIfNotPresent": "true",
                        }
                    ] if i == 0 else []
                }
            })

        carbon_copies = []
        for rec in env.cc:
            recipientId += 1
            carbon_copies.append({
                "email": rec.email,
                "name": rec.name,
                "recipientId": recipientId,
                "routingOrder": 1,
                "emailNotification": {
                    "emailSubject": env.subject,
                },
            })

        for rec in env.cc_completed:
            recipientId += 1
            carbon_copies.append({
                "email": rec.email,
                "name": rec.name,
                "recipientId": recipientId,
                "routingOrder": 2,
                "emailNotification": {
                    "emailSubject": env.subject,
                },
            })

        emailSettings = {}
        if env.reply_to_email is not None and env.reply_to_name is not None:
            emailSettings["replyEmailNameOverride"] = env.reply_to_name
            emailSettings["replyEmailAddressOverride"] = env.reply_to_email

        eventNotification = {}
        if env.webhook is not None:
            eventNotification = {
                "url": env.webhook,
                "loggingEnabled": "true",
                "requireAcknowledgment": "true",
                "useSoapInterface": "false",
                "includeCertificateWithSoap": "false",
                "signMessageWithX509Cert": "false",
                "includeDocuments": "false",
                "includeEnvelopeVoidReason": "true",
                "includeTimeZone": "true",
                "includeSenderAccountAsCustomField": "true",
                "includeDocumentFields": "true",
                "includeCertificateOfCompletion": "false",
                "envelopeEvents": [
                    {"envelopeEventStatusCode": "sent"},
                    {"envelopeEventStatusCode": "delivered"},
                    {"envelopeEventStatusCode": "completed"},
                    {"envelopeEventStatusCode": "declined"},
                    {"envelopeEventStatusCode": "voided"}],
                "recipientEvents": [
                    {"recipientEventStatusCode": "Sent"},
                    {"recipientEventStatusCode": "Delivered"},
                    {"recipientEventStatusCode": "Completed"},
                    {"recipientEventStatusCode": "Declined"},
                    {"recipientEventStatusCode": "AuthenticationFailed"},
                    {"recipientEventStatusCode": "AutoResponded"}]
            }

        data = {
            "documents": [
                {
                    "documentBase64": base64.b64encode(env.document_data).decode("ascii"),
                    "documentId": "1",
                    "name": env.document_name,
                }
            ],
            "emailSubject": env.subject,
            "eventNotification": eventNotification,
            "recipients": {
                "carbonCopies": carbon_copies,
                "signers": signers,
            },
            "emailSettings": emailSettings,
            "status": "sent" if send else "created",
        }
        resp = await self._do_request("/envelopes", data, method="post")
        if "status" in resp and resp["status"] in ["created", "sent"]:
            return resp
        else:
            logging.error(resp)
            raise DocuSignException("unable to create envelope", data, resp)

    async def get_envelope(self, id: str):
        resp = await self._do_request("/envelopes/{0}".format(id), method="get")
        return resp

    async def void_envelope(self, id: str):
        body = {
            "status": "voided",
            "voidedReason": "Contract was voided"
        }
        resp = await self._do_request("/envelopes/{0}".format(id), data=body, method="put")
        return resp

    async def get_envelope_recipients(self, id: str):
        resp = await self._do_request("/envelopes/{0}/recipients".format(id), method="get")
        return resp

    async def update_envelope_recipients(self, id: str, recipients: Any):
        resp = await self._do_request("/envelopes/{0}/recipients".format(id), data=recipients, method="put")
        return resp

    async def create_signing_link(self, id: str, client_user_id: str, recipient_id: str, user_id: str,
                                  ping_url: str = None, ping_frequency: int = 300, return_url: str = None):
        data = {
            "authenticationMethod": "email",
            "clientUserId": client_user_id,
            "userName": client_user_id,
            "email": client_user_id,
            "returnUrl": return_url,
            "recipientId": recipient_id,
            "userId": user_id,
        }

        print(data)
        if ping_url is not None:
            assert 60 <= ping_frequency <= 1200
            data["pingUrl"] = ping_url
            data["pingFrequency"] = str(ping_frequency)
        resp = await self._do_request("/envelopes/{0}/views/recipient".format(id), data=data, method="post")

        print(resp)
        if resp.get("errorCode") == "ENVELOPE_HAS_BEEN_VOIDED":
            raise DocuSignContractVoided()

        return resp["url"]

    async def _do_request(self, url, data=None, method="get"):
        auth = {
            "Username": self.api_username,
            "Password": self.api_password,
            "IntegratorKey": self.api_integrator_key,
        }

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-DocuSign-Authentication": json.dumps(auth),
        }

        async with aiohttp.ClientSession() as session:
            params = None
            if method == "post":
                fn = session.post
                if data is not None:
                    data = json.dumps(data)
            elif method == "put":
                fn = session.put
                if data is not None:
                    data = json.dumps(data)
            elif method == "get":
                fn = session.get
                params = data
            else:
                raise ValueError("unknown method {0}".format(method))

            async with fn(self.api_url + url, headers=headers, data=data, params=params, timeout=60) as response:
                if response.status == 404:
                    raise DocuSignNotFound()

                resp = await response.json()
                return resp


if __name__ == "__main__":
    api = DocuSignAPI()

    cc1 = DocuSignEnvelopeRecipientData()
    cc1.name = "Person1"
    cc1.email = "person1@test-not.comx"

    cc2 = DocuSignEnvelopeRecipientData()
    cc2.name = "Person2"
    cc2.email = "person2@test-not.comx"

    sig1 = DocuSignEnvelopeSignerData()
    sig1.name = "main"
    sig1.email = "main@test-not.comx"

    sig2 = DocuSignEnvelopeSignerData()
    sig2.name = "sig1"
    sig2.email = "sig1@test-not.comx"

    sig3 = DocuSignEnvelopeSignerData()
    sig3.name = "sig2"
    sig3.email = "sig2@test-not.comx"

    env = DocuSignEnvelopeData()
    env.document_name = "dd"
    env.document_data = open("document.pdf", "rb").read()
    env.cc = [cc1, cc2]
    env.main_signer = sig1
    env.clients = [sig2, sig3]
    env.subject = "subject"

    api.login()
    api.create_envelope(env)
