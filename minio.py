from concurrent.futures import ThreadPoolExecutor
import asyncio
import datetime
from urllib.parse import quote

import io
import minio
from minio import Minio


class AsyncMinioClient:
    def __init__(self, host: str, access_key: str, secret_key: str, suffix: str, ioloop=None):
        self.executor = ThreadPoolExecutor(1)
        self.ioloop = ioloop or asyncio.get_event_loop()
        self.suffix = suffix
        self.minio_client = Minio(host,
                                  access_key=access_key,
                                  secret_key=secret_key,
                                  secure=True)

        minio.api._PARALLEL_UPLOADERS = 1

    async def upload_file(self, bucket_name: str, object_name: str, payload: bytes):
        def fn():
            self.minio_client.put_object(bucket_name + self.suffix, object_name, io.BytesIO(payload), len(payload))

        await self.ioloop.run_in_executor(self.executor, fn)

    async def download_file(self, bucket_name: str, object_name: str) -> bytes:
        def fn():
            resp = self.minio_client.get_object(bucket_name + self.suffix, object_name)
            data = resp.read()
            resp.release_conn()
            return data

        return await self.ioloop.run_in_executor(self.executor, fn)

    async def create_download_link(self, bucket_name: str, object_name: str,
                                   download_file_name: str, ttl: datetime.timedelta = datetime.timedelta(days=7)):
        def fn():
            headers = {
                'response-content-disposition': 'attachment; filename="{}"'.format(quote(download_file_name))
            }

            return self.minio_client.presigned_get_object(bucket_name + self.suffix,
                                                          object_name,
                                                          expires=ttl,
                                                          response_headers=headers)

        return await self.ioloop.run_in_executor(self.executor, fn)
