import datetime
import urllib.parse
from decimal import Decimal
from pprint import pprint
from typing import List

import pytz
import aiohttp
import operator


class PayPalTransaction:
    def __init__(self):
        self.transaction_id: str = None
        self.name: str = None
        self.email: str = None
        self.status: str = None
        self.type: str = None
        self.date: datetime.datetime = None
        self.currency_code: str = None
        self.fee_amount: Decimal = None
        self.net_amount: Decimal = None

    def get_total(self):
        return self.net_amount + self.fee_amount

    def __str__(self):
        return "#{} {} {} {}, net: {} fee: {} {}".format(
                self.transaction_id,
                self.date,
                self.type,
                self.name,
                self.net_amount,
                self.fee_amount,
                self.currency_code)

    def __repr__(self):
        return str(self)


class PayPalConversion:
    def __init__(self):
        self.id = None
        self.date = None
        self.source_currency = None
        self.source_amount = None
        self.target_currency = None
        self.target_amount = None

    def __str__(self):
        return "#{} {} {} -> {} {}".format(
                self.id,
                self.date,
                self.source_amount,
                self.source_currency,
                self.target_amount,
                self.target_currency)

    def __repr__(self):
        return str(self)


class PayPalWithdrawn:
    def __init__(self):
        self.date = None
        self.currency = None
        self.amount = None

    def __str__(self):
        return "#{} {} {}".format(
                self.date,
                self.currency,
                self.amount)

    def __repr__(self):
        return str(self)


class PayPalTransactionsCollection:
    def __init__(self, iterable=None):
        self.transactions = []  # type: List[PayPalTransaction]
        if iterable is not None:
            self.transactions = list(iterable)

    def is_valid(self):
        return not any(x is None for x in self.transactions)

    def add(self, tr):
        self.transactions.append(tr)

    def find_by_id(self, id):
        for x in self.transactions:
            if x.transaction_id == id:
                return x
        return None

    def get_dates(self):
        return list(set(x.date.date() for x in self.transactions))

    def get_total(self):
        return sum(x.get_total() for x in self.transactions)

    def remove_all(self, coll: 'PayPalTransactionsCollection'):
        for tr in coll.transactions:
            x = self.find_by_id(tr.transaction_id)
            self.transactions.remove(x)

    def empty(self):
        return len(self.transactions) == 0

    def remove_by_id(self, id):
        x = self.find_by_id(id)
        self.transactions.remove(x)

    def fix(self):
        self.transactions = list(sorted(self.transactions, key=operator.attrgetter("date")))


class TooManyResultsException(Exception):
    pass


class PayPal:
    def __init__(self, username, password, signature, timezone=None):
        self.username = username
        self.password = password
        self.signature = signature
        self.timezone = pytz.timezone(timezone) if timezone is not None else pytz.utc

    async def _do_request(self, data):
        data["USER"] = self.username
        data["PWD"] = self.password
        data["SIGNATURE"] = self.signature
        data["VERSION"] = "94"

        async with aiohttp.ClientSession() as session:
            async with session.post('https://api-3t.paypal.com/nvp', data=data) as response:
                return await response.text()

    async def _do_search_request(self, cls, start_date, end_date):
        if isinstance(start_date, datetime.date):
            start_date = datetime.datetime(year=start_date.year, month=start_date.month, day=start_date.day)
        if isinstance(end_date, datetime.date):
            end_date = datetime.datetime(year=end_date.year, month=end_date.month, day=end_date.day)

        start_date = self.timezone.localize(start_date, self.timezone)
        if end_date is not None:
            end_date = self.timezone.localize(end_date, self.timezone)
        data = {
            "METHOD": "TransactionSearch",
            "TRANSACTIONCLASS": cls,
            "STARTDATE": date_to_paypal(start_date),
        }
        if end_date is not None:
            data["ENDDATE"] = date_to_paypal(end_date)

        resp = await self._do_request(data)
        r = urllib.parse.parse_qs(resp)

        if r["ACK"][0] != "Success":
            error_code = r.get("L_ERRORCODE0")
            if error_code is not None and error_code[0] == "11002":
                raise TooManyResultsException()
            else:
                pprint(r)
                raise Exception(r["ACK"][0])

        transactions = []
        for k in r.keys():
            if k.startswith("L_NAME"):
                num = k[6:]

                timestamp = r["L_TIMESTAMP" + num][0]
                timezone = r["L_TIMEZONE" + num][0]

                date = date_from_paypal(timestamp, timezone)

                tran = PayPalTransaction()
                tran.transaction_id = r["L_TRANSACTIONID" + num][0]
                tran.name = r["L_NAME" + num][0]
                if "L_EMAIL" + num in r:
                    tran.email = r["L_EMAIL" + num][0]
                tran.status = r["L_STATUS" + num][0]
                tran.type = r["L_TYPE" + num][0]
                tran.date = date.astimezone(self.timezone)
                tran.currency_code = r["L_CURRENCYCODE" + num][0]
                tran.fee_amount = -Decimal(r["L_FEEAMT" + num][0])
                tran.net_amount = Decimal(r["L_NETAMT" + num][0])

                transactions.append(tran)

        return list(sorted(transactions, key=operator.attrgetter("date")))

    async def withdrawns(self, start_date: datetime.datetime, end_date: datetime.datetime):
        records = await self._do_search_request("FundsWithdrawn", start_date, end_date)
        out = []
        for rec in records:
            w = PayPalWithdrawn()
            w.date = rec.date
            w.currency = rec.currency_code
            w.amount = rec.net_amount
            out.append(w)
        return out

    async def conversions(self, start_date: datetime.datetime, end_date: datetime.datetime):
        records = await self._do_search_request("CurrencyConversions", start_date, end_date)
        conv = []
        for i in range(0, len(records), 2):
            t1 = records[i]
            t2 = records[i + 1]
            if t1.net_amount > 0:
                t1, t2 = t2, t1

            c = PayPalConversion()
            c.id = t1.transaction_id
            c.date = t1.date
            c.source_amount = t1.net_amount
            c.source_currency = t1.currency_code
            c.target_amount = t2.net_amount
            c.target_currency = t2.currency_code
            conv.append(c)
        return conv

    async def transaction_search(self,
                                 start_date: datetime.datetime,
                                 end_date: datetime.datetime) -> List[PayPalTransaction]:
        return await self._do_search_request("Received", start_date, end_date)

    async def payments_sent_search(self,
                                   start_date: datetime.datetime,
                                   end_date: datetime.datetime) -> List[PayPalTransaction]:
        return await self._do_search_request("Sent", start_date, end_date)

    async def get_transaction_details(self, transaction_id: str):
        data = {
            "METHOD": "GetTransactionDetails",
            "TRANSACTIONID": transaction_id,
        }

        resp = await self._do_request(data)
        r = urllib.parse.parse_qs(resp)
        # assert r["ACK"][0] == "Success"
        return r


def date_to_paypal(date):
    date = date.astimezone(pytz.utc)
    return date.strftime("%Y-%m-%dT%H:%M:%SZ")


def date_from_paypal(date, timezone):
    date_paypal = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%SZ")
    date_wtz = pytz.timezone(timezone).localize(date_paypal, is_dst=None)
    return date_wtz
