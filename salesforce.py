import datetime
import logging
import json
from typing import Tuple, List

import pytz
import requests
import simplejson
from enum import IntEnum

import lib.utils
from lib import date_utils
from common import db
from models import SFContact

log = logging.getLogger("api.salesforce")


class SalesforceAPIRequestException(Exception):
    def __init__(self, resp, resp_data=None):
        super().__init__(resp)
        self.resp = resp
        self.resp_data = resp_data

    def get_sentry_extra(self):
        d = {
            "resp": self.resp,
        }
        if self.resp_data is not None:
            d["resp_data"] = self.resp_data
        return d


class SFUser:
    def __init__(self, id, name, email):
        self.id = id
        self.name = name
        self.email = email

    def __str__(self):
        return "#{0} {1} {2}".format(self.id, self.name, self.email)

    def __repr__(self):
        return str(self)

    def __lt__(self, other):
        return self.name < other.name


class SFField:
    def __init__(self, data):
        self.type = data["dataType"]
        self.id = data["id"]
        self.name = data["name"].strip().lower()
        self.list_options = {x["id"]: x["display"] for x in data["listOptions"]}
        self.multiselect = data["isMultiSelect"]

    def get_value(self, raws):
        if self.type == "DateTime":
            d = datetime.datetime.fromtimestamp(int(raws[0]) / 1000, tz=pytz.utc)
            d = d.astimezone(pytz.timezone('US/Pacific'))
            return d
        elif self.type == "Numeric":
            return str(raws[0])
        elif self.type == "Contact":
            return str(raws[0])
        elif self.type == "List":
            if self.multiselect:
                return [self.list_options[x] for x in raws]
            else:
                if raws[0] in self.list_options:
                    return self.list_options[raws[0]]
                else:
                    return None
        elif self.type == "User":
            return str(raws[0])
        elif self.type == "Text":
            return str(raws[0])
        elif self.type == "ItemLink":
            return str(raws[0])
        else:
            raise ValueError("unsupported value type: {0} value: {1}".format(self.type, raws))

    def __str__(self):
        return "{0} [{1}]".format(self.name, self.type)

    def __repr__(self):
        return str(self)


class SFServiceRequiredEnum(IntEnum):
    Architecture = 0
    Engineering = 1


class SFClientTypeEnum(IntEnum):
    Architect = 0


class SFLeadSourceEnum(IntEnum):
    Affiliate = 24
    AngiesList = 0
    Bing = 16
    BuildZoom = 21
    Craigslist = 1
    FormSpree = 12
    Google = 10
    HomeAdvisor = 2
    Houzz = 3
    Info = 11
    Intercom = 13
    OrganicSearch = 4
    PaidSearch = 5
    Porch = 20
    Referral = 6
    Repeat = 18
    Smith = 22
    Thumbtack = 7
    Unknown = 14
    Upsell = 19
    YellowPages = 8
    Yelp = 9
    RingCentral = 25
    Facebook = 27
    Bark = 31


class SFLeadStatusEnum(IntEnum):
    Lead = 0
    Responded = 15
    Connected = 13
    Negotiation = 2
    YTC = 3
    YTCHigh = 16
    ContractSigned = 11
    RevisitLater = 9
    DealLost = 10
    Unqualified = 12
    Working = 19


class SFDealLostReasonEnum(IntEnum):
    Unresponsive = 12
    TelemarketerEffort = 16
    UnableToServiceClient = 11


class SFAffiliateEnum(IntEnum):
    PeaceOfMind = 13


class SFYTCEnum(IntEnum):
    No = 1
    YTC = 0
    YTCHigh = 2


class SFAPILead:
    def __init__(self):
        self.id = None
        self.creation_date = None
        self.close_date = None
        self.modification_date = None
        self.contract_name = None
        self.lead_source = None
        self.deal_size = None
        self.project_scope = None
        self.csm = None
        self.ytc = None
        self.reason_ytc = None
        self.status = None
        self.lead_quality = None
        self.days_in_current_status = None
        self.client_type = None
        self.service_required = None
        self.affiliate = None


class SalesforceAPI:
    no_add = False

    def __init__(self, api_key: str, api_secret: str, db_session=None):
        self.api_key = api_key
        self.api_secret = api_secret
        self.db_session = db_session or db.session

    @staticmethod
    def set_no_add(no_add):
        SalesforceAPI.no_add = no_add

    def print_fields(self):
        log.info("loading list config...")
        resp = requests.get("https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/",
                            auth=(self.api_key, self.api_secret))
        data = resp.json()
        print(json.dumps(data, indent=True))

    def add_account(self, name, primary_contact_id):
        if len(name) == 0:
            name = "(unknown)"

        data = {
            "name": name,
            "fieldValues": {
                "primary_contact": [{"raw": primary_contact_id}],
                "person_ids": [
                    {"raw": primary_contact_id},
                ],
            }
        }

        data = self._do_request("https://api.salesforceiq.com/v2/accounts", data)
        log.debug("add account data: {0}".format(data))
        return data["id"]

    def update_account(self, account_id, name=None):
        data = {
            "id": account_id,
            "fieldValues": {}
        }

        if name is not None:
            data["name"] = name

        data = self._do_request("https://api.salesforceiq.com/v2/accounts/{0}".format(account_id), data, method="put")
        log.debug("update account data: {0}".format(data))
        return data["id"]

    def add_contact(self, name, email=None, phone=None, address=None, company=None):
        if len(name) == 0:
            name = "(unknown)"

        data = {
            "properties": {
                "name": [{"value": name}],
            },
        }

        if email is not None:
            data["properties"]["email"] = [{"value": email}]
        if phone is not None:
            data["properties"]["phone"] = [{"value": phone}]
        if address is not None:
            data["properties"]["address"] = [{"value": address}]
        if company is not None:
            data["properties"]["company"] = [{"value": company}]

        log.info("adding new contact: {}, email: {}, phone: {}, address: {}".format(name, email, phone, address))
        data = self._do_request("https://api.salesforceiq.com/v2/contacts", data)
        # log.debug("add contact data: {0}".format(data))
        return data["id"]

    def update_contact(self, contact_id, name=None, email=None, phone=None, address=None):
        data = {
            "id": contact_id,
            "properties": {},
        }

        if name is not None:
            data["properties"]["name"] = [{"value": name}]
        if email is not None:
            data["properties"]["email"] = [{"value": email}]
        if phone is not None:
            data["properties"]["phone"] = [{"value": phone}]
        if address is not None:
            data["properties"]["address"] = [{"value": address}]

        log.info("updating contact: #{} {}, email: {}, phone: {}, address: {}".format(contact_id, name, email, phone,
                                                                                      address))
        data = self._do_request("https://api.salesforceiq.com/v2/contacts/{0}".format(contact_id), data, method="put")
        # log.debug("update contact data: {0}".format(data))
        return data["id"]

    def update_lead_contact(self, sf_lead_id: str, name=None, email=None, phone=None, address=None):
        original_lead = self._do_request(
                "https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/listitems/{0}".format(sf_lead_id),
                method="get")

        primary_contact_id = original_lead["fieldValues"]["8"][0]["raw"]

        self.update_contact(primary_contact_id, name=name, email=email, phone=phone, address=address)

    def _create_field_values(self, csm_id: str = None, project_details: str = None, contact_id: str = None,
                             lead_source: SFLeadSourceEnum = None, lead_status: SFLeadStatusEnum = None,
                             client_type: SFClientTypeEnum = None,
                             service_required: SFServiceRequiredEnum = None, affiliate: SFAffiliateEnum = None,
                             ytc: SFYTCEnum = None,
                             deal_list_reason: SFDealLostReasonEnum = None,
                             tracking: str = None,
                             created_date: datetime.datetime = None):
        data = {}
        if csm_id is not None:
            data["5"] = [{"raw": csm_id}]

        if project_details is not None:
            data["15"] = [{"raw": project_details}]

        if contact_id is not None:
            data["8"] = [{"raw": contact_id}]
            data["16"] = [{"raw": contact_id}]

        if lead_source is not None:
            data["13"] = [{"raw": lead_source}]

        if lead_status is not None:
            data["0"] = [{"raw": lead_status}]

        if client_type is not None:
            data["47"] = [{"raw": client_type}]

        if service_required is not None:
            data["48"] = [{"raw": service_required}]

        if affiliate is not None:
            data["53"] = [{"raw": affiliate}]

        if ytc is not None:
            data["45"] = [{"raw": ytc}]

        if deal_list_reason is not None:
            data["52"] = [{"raw": deal_list_reason}]

        if tracking is not None:
            data["66"] = [{"raw": tracking}]

        if created_date is not None:
            data["process_created_date"] = [{"raw": int(date_utils.convert_to_utc(created_date).timestamp() * 1000)}]

        return data

    def add_lead(self, opportunity, csm_id: str = None, project_details: str = None, contact_id: str = None,
                 account_id: str = None,
                 lead_source: SFLeadSourceEnum = None, lead_status: SFLeadStatusEnum = None,
                 client_type: SFClientTypeEnum = None,
                 service_required: SFServiceRequiredEnum = None, affiliate: SFAffiliateEnum = None,
                 deal_lost_reason: SFDealLostReasonEnum = None, ytc: SFYTCEnum = None, tracking: str = None):

        if lib.utils.debug:
            opportunity = "KDTEST " + opportunity

        data = {
            "name": opportunity,
            "accountId": account_id,
            "contactIds": [contact_id],
            "fieldValues": self._create_field_values(
                    csm_id=csm_id,
                    project_details=project_details,
                    contact_id=contact_id,
                    lead_source=lead_source,
                    lead_status=lead_status,
                    client_type=client_type,
                    service_required=service_required,
                    ytc=ytc,
                    affiliate=affiliate,
                    deal_list_reason=deal_lost_reason,
                    tracking=tracking,
            )
        }

        log.info("adding new lead: /{}/, source: {}, status: {}".format(opportunity, lead_source, lead_status))
        data = self._do_request("https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/listitems", data)
        # log.debug("add lead data: {0}".format(data))
        return data["id"]

    def update_lead(self, lead_id: str, opportunity: str = None, csm_id: str = None, project_details: str = None,
                    contact_id: str = None, contact_ids: List[str] = None, account_id: str = None,
                    lead_source: SFLeadSourceEnum = None, lead_status: SFLeadStatusEnum = None,
                    client_type: SFClientTypeEnum = None,
                    service_required: SFServiceRequiredEnum = None, affiliate: SFAffiliateEnum = None,
                    deal_lost_reason: SFDealLostReasonEnum = None, ytc: SFYTCEnum = None,
                    created_date: datetime = None, tracking: str = None):
        data = {
            "fieldValues": self._create_field_values(
                    csm_id=csm_id,
                    project_details=project_details,
                    contact_id=contact_id,
                    lead_source=lead_source,
                    lead_status=lead_status,
                    client_type=client_type,
                    service_required=service_required,
                    ytc=ytc,
                    affiliate=affiliate,
                    deal_list_reason=deal_lost_reason,
                    tracking=tracking,
                    created_date=created_date,
            )
        }

        original_lead = self._do_request(
                "https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/listitems/{0}".format(lead_id),
                method="get")

        data["accountId"] = original_lead["accountId"]
        data["contactIds"] = original_lead["contactIds"]

        # if created_date is not None:
        #    data["createdDate"] = date_utils.convert_to_utc(created_date).strftime("%s")

        if opportunity is not None:
            data["name"] = opportunity

        data = self._do_request(
                "https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/listitems/{0}".format(lead_id), data,
                method="put")
        # log.debug("update lead data: {0}".format(data))
        return data["id"]

    def _do_request(self, url, data=None, method="post"):
        headers = {
            "Content-Type": "application/json"
        }

        if method != "get" and SalesforceAPI.no_add:
            return {"id": "skip"}

        params = None
        if method == "post":
            fn = requests.post
            if data is not None:
                data = json.dumps(data)
        elif method == "put":
            fn = requests.put
            if data is not None:
                data = json.dumps(data)
        elif method == "get":
            fn = requests.get
            params = data
        else:
            raise ValueError("unknown method {0}".format(method))

        resp = fn(url, headers=headers, data=data, params=params, timeout=120, auth=(self.api_key, self.api_secret))
        try:
            json_data = resp.json()
        except simplejson.JSONDecodeError:
            raise SalesforceAPIRequestException(resp, resp.text)

        if "errorMessage" in json_data:
            raise SalesforceAPIRequestException(resp, json_data)

        return json_data

    def add_lead_with_contact(self,
                              opportunity: str,
                              contact_name: str,
                              contact_address: str = None,
                              contact_email: str = None,
                              contact_phone: str = None,
                              contact_company: str = None,
                              csm_id: str = None,
                              project_details: str = None,
                              lead_source: SFLeadSourceEnum = None,
                              lead_status: SFLeadStatusEnum = None,
                              client_type: SFClientTypeEnum = None,
                              service_required: SFServiceRequiredEnum = None,
                              affiliate: SFAffiliateEnum = None,
                              deal_lost_reason: SFDealLostReasonEnum = None, ytc: SFYTCEnum = None,
                              tracking: str = None) -> Tuple[
        SFContact, str]:

        if SalesforceAPI.no_add:
            return SFContact(), "lid"

        dbcontact = self.create_sf_contact(name=contact_name, address=contact_address, email=contact_email,
                                           phone=contact_phone, company=contact_company)

        sf_lead_id = self.add_lead(
                opportunity=opportunity,
                lead_source=lead_source,
                project_details=project_details,
                contact_id=dbcontact.sf_contact_id,
                account_id=dbcontact.sf_account_id,
                csm_id=csm_id,
                lead_status=lead_status,
                client_type=client_type,
                deal_lost_reason=deal_lost_reason,
                service_required=service_required,
                tracking=tracking,
                ytc=ytc,
                affiliate=affiliate)
        log.info("lead created: {0}".format(sf_lead_id))

        return dbcontact, sf_lead_id

    def create_sf_contact(self, name: str, address: str = None, email: str = None, phone: str = None,
                          company: str = None) -> SFContact:
        # finding in local database
        # dbcontact = self.find_sf_contact_by_name(name)
        # if dbcontact is not None:
        #    return dbcontact

        # trying to add to SF
        sf_contact_id = self.add_contact(name=name, address=address, email=email, phone=phone, company=company)

        dbcontact = self.db_session.query(SFContact).filter(SFContact.sf_contact_id == sf_contact_id).one_or_none()
        if dbcontact is not None:
            log.info("using existing SF contact {0}...".format(name))
            return dbcontact

        dbcontact = SFContact(name=name, email=email, address=address, phone=phone)

        log.info("creating SF contact {0}...".format(name))
        dbcontact.sf_contact_id = sf_contact_id
        log.info("contact created: {0}, creating SF account...".format(dbcontact.sf_contact_id))
        log.info("creating SF account...")
        dbcontact.sf_account_id = self.add_account(name, dbcontact.sf_contact_id)
        log.info("account created: {0}, creating SF lead...".format(dbcontact.sf_account_id))
        self.db_session.add(dbcontact)
        self.db_session.commit()
        return dbcontact

    def get_fields_raw(self):
        resp = self._do_request("https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/", method="get")
        return resp

    def get_leads_raw_objects(self, modification_date: int = None):
        cur_idx = 0
        new_cnt = 0
        raw_objects = []
        while True:
            data = {'_start': cur_idx, '_limit': 200}
            if modification_date is not None:
                data["modifiedDate"] = modification_date  # - 1000 * 60 * 5

            log.info("loading leads ({0})...".format(cur_idx))
            data = self._do_request("https://api.salesforceiq.com/v2/lists/575c82ede4b0db1037e4c659/listitems/",
                                    data=data, method="get")
            objects = data["objects"]

            for obj in objects:
                if modification_date is not None and obj["modifiedDate"] < modification_date:
                    continue

                # required_fields = ["process_close_date"]
                # for f in required_fields:
                #     assert f in obj["fieldValues"]

                raw_objects.append(obj)
                new_cnt += 1
            log.info("got {0} leads, {1} new/modified".format(len(objects), new_cnt))

            if len(objects) < 200:
                break
            cur_idx += len(objects)

        return raw_objects

    def get_contacts_raw_objects(self, modification_date: int = None):
        cur_idx = 0
        new_cnt = 0
        raw_objects = []
        while True:
            data = {'_start': cur_idx, '_limit': 200}
            if modification_date is not None:
                data["modifiedDate"] = modification_date  # - 1000 * 60 * 5

            log.info("loading contacts ({0})...".format(cur_idx))
            data = self._do_request("https://api.salesforceiq.com/v2/contacts", data=data, method="get")
            objects = data["objects"]

            for obj in objects:
                if obj is None:
                    continue
                if modification_date is not None and obj["modifiedDate"] < modification_date:
                    continue
                raw_objects.append(obj)
                new_cnt += 1
            log.info("got {0} contacts, {1} new/modified".format(len(objects), new_cnt))

            if len(objects) < 200:
                break
            cur_idx += len(objects)

        return raw_objects

    def get_accounts_raw_objects(self, modification_date: int = None):
        cur_idx = 0
        new_cnt = 0
        raw_objects = []
        while True:
            data = {'_start': cur_idx, '_limit': 200}
            if modification_date is not None:
                data["modifiedDate"] = modification_date  # - 1000 * 60 * 5

            log.info("loading accounts ({0})...".format(cur_idx))
            data = self._do_request("https://api.salesforceiq.com/v2/accounts", data=data, method="get")
            objects = data["objects"]

            for obj in objects:
                if modification_date is not None and obj["modifiedDate"] < modification_date:
                    continue
                raw_objects.append(obj)
                new_cnt += 1
            log.info("got {0} accounts, {1} new/modified".format(len(objects), new_cnt))

            if len(objects) < 200:
                break
            cur_idx += len(objects)

        return raw_objects

        # mod_date = datetime.datetime.fromtimestamp(obj["modifiedDate"] / 1000.0, tz=pytz.utc)
