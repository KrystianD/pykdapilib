import json
import logging
from typing import Any, List, Tuple, Dict

import aiohttp
import enum


class SendInBlueAPI:
    def __init__(self, key: str):
        self.key = key

    async def send_template(self,
                            to: List[str],
                            cc: List[str],
                            template_id: int,
                            parameters: Dict[str, str]):
        data = {
            "emailTo": to,
            "attributes": parameters,
        }

        if cc is not None and len(cc) > 0:
            data["emailCc"] = cc

        async with aiohttp.ClientSession() as session:
            async with session.post(f'https://api.sendinblue.com/v3/smtp/templates/{template_id}/send',
                                    json=data,
                                    headers={
                                        "api-key": self.key,
                                    }) as response:
                return await response.json()
