import json
import logging
from typing import Any, List, Tuple

import aiohttp
import enum


class SlackAPIButtonStyle(enum.Enum):
    Default = "default"
    Primary = "primary"
    Danger = "danger"


class SlackAPIMessageAttachment:
    def __init__(self):
        self.text: str = None
        self.title: str = None
        self.callback_id: str = None
        self.actions = []
        self.fields = []
        self.footer: str = None
        self.footer_icon: str = None
        self.color: str = None

    def add_field(self, label: str, content: str, short: bool = True):
        self.fields.append({
            "title": label,
            "value": content,
            "short": short,
        })

    def add_action(self, name: str, value: str, label: str, style=SlackAPIButtonStyle.Default):
        self.actions.append({
            "name": name,
            "text": label,
            "value": value,
            "type": "button",
            "style": style.value,
        })
        """
        "confirm": {
            "title": "Are you sure?",
            "text": "some text",
            "ok_text": "Yes",
            "dismiss_text": "No"
        }
        """
        return self

    def add_menu(self, name: str, label: str, options: List[Tuple[str, str]]):
        self.actions.append({
            "name": name,
            "text": label,
            "type": "select",
            "options": [{"text": x[0], "value": x[1]} for x in options],
        })

    def to_dict(self):
        return {
            "text": self.text,
            "title": self.title,
            "fallback": "",
            "callback_id": self.callback_id,
            "attachment_type": "default",
            "actions": self.actions,
            "fields": self.fields,
            "footer": self.footer,
            "footer_icon": self.footer_icon,
            "color": self.color,
        }


class SlackAPIMessage:
    def __init__(self, text: str) -> None:
        self.text = text
        self.attachments: List[SlackAPIMessageAttachment] = []
        self.user_name: str = None
        self.user_avatar: str = None

    def add_attachment(self, text: str = "", callback_id: str = None, title=None, color: str = None):
        a = SlackAPIMessageAttachment()
        a.title = title
        a.text = text
        a.callback_id = callback_id
        a.color = color
        self.attachments.append(a)
        return a


def get_last(*args):
    for x in args:
        if x is not None:
            return x


class SlackAPI:
    def __init__(self, token: str, sender_name: str = None, sender_avatar: str = None):
        self.token = token
        self.sender_name = sender_name
        self.sender_avatar = sender_avatar

    async def post_message(self, channel: str, message: SlackAPIMessage):
        import de_info
        data = {
            "channel": de_info.SLACK_CHANNELS.get(channel, channel),
            "token": self.token,
            "text": message.text,
            "as_user": False,
            "attachments": json.dumps([x.to_dict() for x in message.attachments]),
        }
        username = get_last(self.sender_name, message.user_name)
        user_avatar = get_last(self.sender_avatar, message.user_avatar)

        if username is not None:
            data["username"] = username
        if user_avatar is not None:
            if user_avatar[0] == ":":
                data["icon_emoji"] = user_avatar
            else:
                data["icon_url"] = user_avatar
        await self.cmd("chat.postMessage", data)

    async def cmd(self, method: str, params: Any):
        async with aiohttp.ClientSession() as session:
            async with session.post("https://slack.com/api/{0}".format(method), data=params) as resp:
                if resp.status != 200:
                    logging.error(resp)
                    raise Exception("Error response")

    @staticmethod
    def create_from_config():
        import config
        return SlackAPI(config.SLACK_TOKEN)
